package com.sixteeneyes.daligoapi.model.kickboard;

import com.sixteeneyes.daligoapi.entity.KickBoard.KickBoardHistory;
import com.sixteeneyes.daligoapi.interfaces.CommonModelBuilder;
import com.sixteeneyes.daligoapi.lib.CommonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.temporal.ChronoUnit;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class KickBoardHistoryResponse {
    @ApiModelProperty(notes = "킥보드", required = true)
    private String kickBoard;

    @ApiModelProperty(notes = "시작일", required = true)
    private String dateStart;

    @ApiModelProperty(notes = "종료일", required = true)
    private String dateEnd;

    @ApiModelProperty(notes = "이용 시간", required = true)
    private String timeUse;

    @ApiModelProperty(notes = "기본요금", required = true)
    private Double basePrice;

    @ApiModelProperty(notes = "분당 추가 요금", required = true)
    private Double perMinPrice;

    @ApiModelProperty(notes = "이용요금", required = true)
    private BigDecimal resultPrice;

    private KickBoardHistoryResponse(Builder builder) {
        this.kickBoard = builder.kickBoard;
        this.dateStart = builder.dateStart;
        this.dateEnd = builder.dateEnd;
        this.timeUse = builder.timeUse;
        this.basePrice = builder.basePrice;
        this.perMinPrice = builder.perMinPrice;
        this.resultPrice = builder.resultPrice;
    }

    public static class Builder implements CommonModelBuilder<KickBoardHistoryResponse> {
        private final String kickBoard;
        private final String dateStart;
        private final String dateEnd;
        private final String timeUse;
        private final Double basePrice;
        private final Double perMinPrice;
        private final BigDecimal resultPrice;

        public Builder(KickBoardHistory kickBoardHistory) {
            this.kickBoard = kickBoardHistory.getKickBoard().getUniqueNumber();
            this.dateStart = CommonFormat.convertLocalDateTimeToString(kickBoardHistory.getDateStart());
            this.dateEnd = CommonFormat.convertLocalDateTimeToString(kickBoardHistory.getDateEnd());
            this.timeUse = String.valueOf(Math.ceil(ChronoUnit.SECONDS.between(kickBoardHistory.getDateStart(), kickBoardHistory.getDateEnd()) / 60.0));
            this.basePrice = kickBoardHistory.getKickBoard().getPriceBasis().getBasePrice();
            this.perMinPrice = (Math.ceil(ChronoUnit.SECONDS.between(kickBoardHistory.getDateStart(), kickBoardHistory.getDateEnd()) / 60.0)) * kickBoardHistory.getKickBoard().getPriceBasis().getPerMinPrice();
            this.resultPrice = kickBoardHistory.getResultPrice();
        }
        @Override
        public KickBoardHistoryResponse build() {
            return new KickBoardHistoryResponse(this);
        }
    }
}