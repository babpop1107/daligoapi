package com.sixteeneyes.daligoapi.model.review;

import com.sixteeneyes.daligoapi.entity.Review;
import com.sixteeneyes.daligoapi.interfaces.CommonModelBuilder;
import com.sixteeneyes.daligoapi.lib.CommonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReviewResponse {
    @ApiModelProperty(notes = "이름", required = true)
    private String name;

    @ApiModelProperty(notes = "내용", required = true)
    private String content;

    @ApiModelProperty(notes = "이미지주소", required = true)
    private String imgName;

    @ApiModelProperty(notes = "작성일", required = true)
    private String dateWrite;

    private ReviewResponse(Builder builder) {
        this.name = builder.name;
        this.content = builder.content;
        this.imgName = builder.imgName;
        this.dateWrite = builder.dateWrite;
    }

    public static class Builder implements CommonModelBuilder<ReviewResponse> {
        private final String name;
        private final String content;
        private final String imgName;
        private final String dateWrite;

        public Builder(Review review) {
            this.name = review.getName().charAt(0) + "0".repeat(review.getName().length() - 1);;
            this.content = review.getContent();
            this.imgName = review.getImgName();
            this.dateWrite = CommonFormat.convertLocalDateTimeToString(review.getDateWrite());
        }

        @Override
        public ReviewResponse build() {
            return new ReviewResponse(this);
        }
    }
}
