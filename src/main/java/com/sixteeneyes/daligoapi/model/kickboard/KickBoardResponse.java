package com.sixteeneyes.daligoapi.model.kickboard;

import com.sixteeneyes.daligoapi.entity.KickBoard.KickBoard;
import com.sixteeneyes.daligoapi.enums.KickBoardStatus;
import com.sixteeneyes.daligoapi.interfaces.CommonModelBuilder;
import com.sixteeneyes.daligoapi.lib.CommonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class KickBoardResponse {
    @ApiModelProperty(notes = "킥보드 상태", required = true)
    private String kickBoardStatus;

    @ApiModelProperty(notes = "구입일", required = true)
    private String dateBuy;

    @ApiModelProperty(notes = "위도", required = true)
    private Double posX;

    @ApiModelProperty(notes = "경도", required = true)
    private Double posY;

    @ApiModelProperty(notes = "사용여부", required = true)
    private String isUse;

    private KickBoardResponse(Builder builder) {
        this.kickBoardStatus = builder.kickBoardStatus;
        this.dateBuy = builder.dateBuy;
        this.posX = builder.posX;
        this.posY = builder.posY;
        this.isUse = builder.isUse;
    }

    public static class Builder implements CommonModelBuilder<KickBoardResponse> {
        private final String kickBoardStatus;
        private final String dateBuy;
        private final Double posX;
        private final Double posY;
        private final String isUse;

        public Builder(KickBoard kickBoard) {
            this.kickBoardStatus = kickBoard.getKickBoardStatus().getName();
            this.dateBuy = CommonFormat.convertLocalDateToString(kickBoard.getDateBuy());
            this.posX = kickBoard.getPosX();
            this.posY = kickBoard.getPosY();
            this.isUse = kickBoard.getIsUse() ? "O" : "X";
        }

        @Override
        public KickBoardResponse build() {
            return new KickBoardResponse(this);
        }
    }
}
