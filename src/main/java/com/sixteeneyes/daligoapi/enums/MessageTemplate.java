package com.sixteeneyes.daligoapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MessageTemplate {
    TD_8763("본인 인증 번호", true, "인증번호는 #{인증번호} 입니다.") // 무조건 받아야하니까 true
    , TD_7777("임시 비밀번호 안내", true, "임시비밀번호는 #{임시비번} 입니다.");

    private final String name;
    private final boolean isSendMms;
    private final String contents;
}
