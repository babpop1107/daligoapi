package com.sixteeneyes.daligoapi.model.member;

import com.sixteeneyes.daligoapi.enums.MemberGroup;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MemberJoinRequest extends MemberCreateRequest {
    @ApiModelProperty(notes = "회원 그룹", required = true)
    @NotNull
    @Enumerated(EnumType.STRING)
    private MemberGroup memberGroup;
}
