package com.sixteeneyes.daligoapi.entity;

import com.sixteeneyes.daligoapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MessageSendHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 13)
    private String sendPhoneNumber;

    @Column(nullable = false, length = 13)
    private String receivePhoneNumber;

    @Column(nullable = false, columnDefinition = "TEXT")
    private String messageContents;

    @Column(nullable = false)
    private LocalDateTime dateSend;

    private MessageSendHistory(Builder builder) {
        this.sendPhoneNumber = builder.sendPhoneNumber;
        this.receivePhoneNumber = builder.receivePhoneNumber;
        this.messageContents = builder.messageContents;
        this.dateSend = builder.dateSend;
    }

    public static class Builder implements CommonModelBuilder<MessageSendHistory> {

        private final String sendPhoneNumber;
        private final String receivePhoneNumber;
        private final String messageContents;
        private final LocalDateTime dateSend;

        public Builder(String sendPhoneNumber, String receivePhoneNumber, String messageContents) {
            this.sendPhoneNumber = sendPhoneNumber;
            this.receivePhoneNumber = receivePhoneNumber;
            this.messageContents = messageContents;
            this.dateSend = LocalDateTime.now();
        }
        @Override
        public MessageSendHistory build() {
            return new MessageSendHistory(this);
        }
    }
}
