package com.sixteeneyes.daligoapi.repository;

import com.sixteeneyes.daligoapi.entity.MessageSendHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageSendHistoryRepository extends JpaRepository<MessageSendHistory, Long> {
}
