package com.sixteeneyes.daligoapi.model.kickboard;

import com.sixteeneyes.daligoapi.entity.KickBoard.KickBoardHistory;
import com.sixteeneyes.daligoapi.interfaces.CommonModelBuilder;
import com.sixteeneyes.daligoapi.lib.CommonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.temporal.ChronoUnit;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class KickBoardHistoryItem {
    @ApiModelProperty(notes = "시퀀스", required = true)
    private Long id;

    @ApiModelProperty(notes = "킥보드", required = true)
    private String kickBoard;

    @ApiModelProperty(notes = "사용 시작 시간", required = true)
    private String dateStart;

    @ApiModelProperty(notes = "이용 시간", required = true)
    private String timeUse;

    @ApiModelProperty(notes = "차감 금액", required = true)
    private BigDecimal resultPrice;

    private KickBoardHistoryItem(Builder builder) {
        this.id = builder.id;
        this.kickBoard = builder.kickBoard;
        this.dateStart = builder.dateStart;
        this.timeUse = builder.timeUse;
        this.resultPrice = builder.resultPrice;
    }

    public static class Builder implements CommonModelBuilder<KickBoardHistoryItem> {
        private final Long id;
        private final String kickBoard;
        private final String dateStart;
        private final String timeUse;
        private final BigDecimal resultPrice;

        public Builder(KickBoardHistory kickBoardHistory) {
            this.id = kickBoardHistory.getId();
            this.kickBoard = kickBoardHistory.getKickBoard().getUniqueNumber();
            this.dateStart = CommonFormat.convertLocalDateTimeToString(kickBoardHistory.getDateStart());
            this.timeUse = String.valueOf(Math.ceil(ChronoUnit.SECONDS.between(kickBoardHistory.getDateStart(), kickBoardHistory.getDateEnd()) / 60.0));
            this.resultPrice = kickBoardHistory.getResultPrice();
        }

        @Override
        public KickBoardHistoryItem build() {
            return new KickBoardHistoryItem(this);
        }
    }
}