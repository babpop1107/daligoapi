package com.sixteeneyes.daligoapi.entity;

import com.sixteeneyes.daligoapi.entity.Member.Member;
import com.sixteeneyes.daligoapi.interfaces.CommonModelBuilder;
import com.sixteeneyes.daligoapi.lib.CommonFormat;
import com.sixteeneyes.daligoapi.model.AmountChargeRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AmountChargingHistory {
    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 회원
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    // 충전날짜
    @Column(nullable = false)
    private LocalDateTime dateCharge;

    // 충전금액
    @Column(nullable = false)
    private BigDecimal price;

    private AmountChargingHistory(Builder builder) {
        this.member = builder.member;
        this.dateCharge = builder.dateCharge;
        this.price = builder.price;
    }

    public static class Builder implements CommonModelBuilder<AmountChargingHistory> {
        private final Member member;
        private final LocalDateTime dateCharge;
        private final BigDecimal price;

        public Builder(Member member, double price) {
            this.member = member;
            this.dateCharge = LocalDateTime.now();
            this.price = CommonFormat.convertDoubleToDecimal(price);
        }

        @Override
        public AmountChargingHistory build() {
            return new AmountChargingHistory(this);
        }
    }
}
